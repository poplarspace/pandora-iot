/* 文件名称：主任务循环头文件
 * 文件说明：该文件中声明了TIM相关操作的函数以及变量等 */
#ifndef __BSP_TIM_H__
#define __BSP_TIM_H__
// 头文件
#include "stdint.h"
#include "main.h"
#include "tim.h"



// 外部变量声明
extern uint32_t timer6_count;



// 函数声明
void USER_TIM_IRQHandler0(TIM_HandleTypeDef *htim);
  


#endif   // end of __BSP_TIM_H__



