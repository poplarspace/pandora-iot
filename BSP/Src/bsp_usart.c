/* 文件名称：主任务循环源文件
 * 文件说明：该文件中定义了USART相关操作的函数以及变量等 */
#include "bsp_usart.h"
#include "main.h"
#include "stdio.h"  // 包含标准输入输出头文件，用于重定向printf
#include "usart.h"



/// @brief 重定向c库函数printf到串口
/// @param ch 要写入的字符
/// @param f 指向FILE结构的指针
/// @return 要写入的字符
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
    while(__HAL_UART_GET_FLAG(&huart1, UART_FLAG_TC)!=SET);		
    return ch;
}

/// @brief 重定向c库函数scanf到串口
/// @param f 指向FILE结构的指针
/// @return 输入的字符
int fgetc(FILE *f)
{
    int ch;
    
    // 等待串口输入数据
    while(__HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE) == RESET);
    HAL_UART_Receive(&huart1, (uint8_t*)&ch, 1, 0xFFFF);
    return ch;
}


