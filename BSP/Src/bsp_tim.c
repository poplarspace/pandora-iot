/* 文件名称：主任务循环源文件
 * 文件说明：该文件中定义了TIMER相关操作的函数以及变量等 */
#include "bsp_tim.h"
#include "key_check.h"



// 定义全局变量
// 定时器相关变量 
uint32_t timer6_count   = 0;  
// 按键检测相关变量
uint32_t key_check_time = 0; 



/// @brief TIMER溢出事件回调函数 
/// @param htim 定时器处理参数结构体
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // Time6计数值，每1ms计数一次 
    if(htim->Instance == TIM6)
    {
        // 计数值递增
        timer6_count++;

        // 每10ms检测1次
        if(key_check_time > 10)
        {
            // 计数值清零 
            key_check_time = 0;
            // 检测按键状态
            key_EventCheck();
        }
        else
        {
            key_check_time++;
        }
    }
}

