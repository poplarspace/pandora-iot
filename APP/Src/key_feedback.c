/* 文件简介：按键事件反馈源文件
 * 文件说明：该文件中定义了按键事件反馈相关的函数和变量 */
#include "key_feedback.h"
#include "stdio.h"



/// @brief 按键0按下反馈反馈函数
/// @param 无 
void key_Key0PressFeedback(void)
{
    printf("按下按键0\n");
}

/// @brief 按键0长按反馈反馈函数
/// @param 无 
void key_Key0LongPressFeedback(void)
{
    printf("长按按键0\n");
}

/// @brief 按键1按下反馈反馈函数
/// @param 无 
void key_Key1PressFeedback(void)
{
    printf("按下按键1\n");
}

/// @brief 按键1长按反馈反馈函数
/// @param 无 
void key_Key1LongPressFeedback(void)
{
    printf("长按按键1\n");
}

/// @brief 按键2按下反馈反馈函数
/// @param 无 
void key_Key2PressFeedback(void)
{
    printf("按下按键2\n");
}

/// @brief 按键2长按反馈反馈函数
/// @param 无 
void key_Key2LongPressFeedback(void)
{
    printf("长按按键2\n");
}


