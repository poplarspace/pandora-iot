/* 文件名称：主任务循环源文件
 * 文件说明：该文件中定义了LED相关操作的函数以及变量等 */
#include "led.h"
#include "stm32l4xx_hal.h"
#include "main.h"



/// @brief 控制显示led的状态
/// @param led_colour led颜色枚举值
/// @param led_status led状态枚举值
void led_Show(enum ledClour led_colour, enum ledStatus led_status)
{
    switch(led_colour){
        case RED:
            if(OPEN == led_status){
                HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
            }else{
                HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
            }
            break;
        case GREEN:
            if(OPEN == led_status){
                HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
            }else{
                HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
            }
            break;
        case BLUE:
            if(OPEN == led_status){
                HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
            }else{
                HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_SET);
            }
            break;
    }
}


