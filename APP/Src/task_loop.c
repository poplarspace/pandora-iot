/* 文件名称：主任务循环源文件
 * 文件说明：该文件中定义了主任务循环相关操作的函数以及变量等 */
#include "task_loop.h"
#include "key_handle.h"
#include "bsp_tim.h"
#include "stdio.h"



/// @brief 任务初始化
/// @param 无 
void task_Init(void)
{
    // 按键初始化
    key_Init();
    
    // 打开定时器
    HAL_TIM_Base_Start_IT(&htim6);  // 开启定时器6
}

/// @brief 任务循环
/// @param 无 
void task_Loop(void)
{
    // 按键事件轮询处理
    key_EventLoop();
}



