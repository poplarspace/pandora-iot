/* 文件简介：按键事件处理源文件
 * 文件说明：该文件中定义了按键事件处理相关的函数和变量 */
#include "key_handle.h"
#include "key_feedback.h"



// 单一按键事件
struct keyHandleParam   key0_handle;            // 定义按键0事件处理参数结构体
struct keyHandleParam   key1_handle;            // 定义按键1事件处理参数结构体
struct keyHandleParam   key2_handle;            // 定义按键2事件处理参数结构体



/// @brief 按键事件处理轮询函数
/// @param 无  
void key_EventLoop(void)
{ 
    // 单一按键事件轮询处理
    key_EventHandle(key0_check, &key0_handle);  // 按键0
    key_EventHandle(key1_check, &key1_handle);  // 按键1 
    key_EventHandle(key2_check, &key2_handle);  // 按键2  
}

/// @brief 按键初始化函数
/// @param 无 
void key_Init(void)
{
    // 按键检测参数初始化
    key_CheckInit();
    
    // 按键处理参数初始化
    key_HandleInit();
}

/// @brief 按键事件处理初始化
/// @param 无 
void key_HandleInit(void)
{
    /* 单一按键参数初始化
       包括结构体参数与反馈函数指针 */
    // 按键0
    key_HandleParamInit(&key0_handle);
    key0_handle.press_feedback           = key_Key0PressFeedback;
    key0_handle.long_press_feedback      = key_Key0LongPressFeedback;
    // 按键1
    key_HandleParamInit(&key1_handle);
    key1_handle.press_feedback           = key_Key1PressFeedback;
    key1_handle.long_press_feedback      = key_Key1LongPressFeedback;   
    // 按键2
    key_HandleParamInit(&key1_handle);
    key2_handle.press_feedback           = key_Key2PressFeedback;
    key2_handle.long_press_feedback      = key_Key2LongPressFeedback; 
}

/// @brief 单一按键事件处理参数结构体初始化
/// @param handle_struc 按键参数结构体指针
void key_HandleParamInit(struct keyHandleParam* handle_struc)
{   
    // 按键事件处理参数初始化
    handle_struc->event_handle_ban       = 0;
    handle_struc->press_handle_flag      = 0;
    handle_struc->long_press_handle_flag = 0;
}

/// @brief 双按键事件处理参数结构体初始化
/// @param double_handle_struc 双按键参数结构体指针
void key_DoubleHandleParamInit(struct keyDoubleHandleParam* double_handle_struc)
{   
    // 按键事件处理参数初始化
    double_handle_struc->double_event_handle_ban       = 0;
    double_handle_struc->double_press_handle_flag      = 0;
    double_handle_struc->double_long_press_handle_flag = 0;
}

/// @brief 按键事件处理函数
/// @param check_param 按键事件检测参数结构体
/// @param handle_param 按键事件处理参数结构体
void key_EventHandle(struct keyCheckParam check_param, struct keyHandleParam* handle_param)
{
    // 按键按下时
    if(PRESS_DOWN == check_param.key_event)
    {
        // 事件处理标志位为0时
        if(!handle_param->press_handle_flag)
        {            
            // 若未禁止则进行处理
            if(!handle_param->event_handle_ban)
            {
                // 按键按下事件反馈函数
                handle_param->press_feedback();
            }
            
            // 事件处理结束，标志位置1
            handle_param->press_handle_flag = 1;
        }
    }
    // 按键长按时
    if(LONG_PRESS == check_param.key_event)
    {
        // 事件处理标志位为0时
        if(!handle_param->long_press_handle_flag)
        {
            // 若未禁止则进行处理
            if(!handle_param->event_handle_ban)
            {
                // 按键长按事件反馈函数
                handle_param->long_press_feedback();
            }

            // 事件处理结束，标志位置1
            handle_param->long_press_handle_flag = 1;
        }
    }
    // 按键松开时
    if(PRESS_UP == check_param.key_event)
    {
        // 重置按键操作标志位
        handle_param->press_handle_flag      = 0;
        handle_param->long_press_handle_flag = 0;
    }        
}

/// @brief 双按键事件处理函数
/// @param check_param1 按键1事件检测参数结构体
/// @param check_param2 按键2事件检测参数结构体
/// @param double_handle_param 双按键事件处理参数结构体
void key_DoubleEventHandle(struct keyCheckParam check_param1, struct keyCheckParam check_param2, 
                           struct keyDoubleHandleParam* double_handle_param)
{
    // 双按键同时按下时
    if(PRESS_DOWN == check_param1.key_event && PRESS_DOWN == check_param2.key_event)
    {
        // 事件处理标志位为0时
        if(!double_handle_param->double_press_handle_flag)
        { 
            // 若未禁止则进行处理
            if(!double_handle_param->double_event_handle_ban)
            {
                // 双按键按下事件反馈函数
                double_handle_param->double_press_feedback();
            }
            
            // 事件处理结束，标志位置1
            double_handle_param->double_press_handle_flag = 1;
        }
    }
    // 双按键同时长按时
    if(LONG_PRESS == check_param1.key_event && LONG_PRESS == check_param2.key_event)
    {
        // 事件处理标志位为0时
        if(!double_handle_param->double_long_press_handle_flag)
        { 
            // 若未禁止则进行处理
            if(!double_handle_param->double_event_handle_ban)
            {
                // 双按键长按事件反馈函数
                double_handle_param->double_long_press_feedback();
            }
            
            // 事件处理结束，标志位置1
            double_handle_param->double_long_press_handle_flag = 1;
        }
    }
    // 任一按键松开时 
    if(PRESS_UP == check_param1.key_event || PRESS_UP == check_param2.key_event)
    {
        // 重置按键操作标志位
        double_handle_param->double_press_handle_flag       = 0;
        double_handle_param->double_long_press_handle_flag  = 0;
    }  
}


