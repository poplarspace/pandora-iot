/* 文件简介：按键检测源文件
 * 文件说明：该文件中定义了按键检测相关的函数和变量 */
#include "key_check.h"



// 定义按键事件检测参数结构体
struct keyCheckParam    key0_check;       // 定义按键1事件检测参数结构体
struct keyCheckParam    key1_check;       // 定义按键1事件检测参数结构体
struct keyCheckParam    key2_check;       // 定义按键2事件检测参数结构体



/// @brief 按键事件检查
/// @param 无 
void key_EventCheck(void)
{
    // 对按键状态进行检测
    key_CheckStatus(KEY0_GPIO, KEY0_GPIO_PIN, &key0_check, MODE_TWO);    // 检测按键0
    key_CheckStatus(KEY1_GPIO, KEY1_GPIO_PIN, &key1_check, MODE_TWO);    // 检测按键1
    key_CheckStatus(KEY2_GPIO, KEY2_GPIO_PIN, &key2_check, MODE_TWO);    // 检测按键2
}

/// @brief 初始化按键
/// @param 无 
void key_CheckInit(void)
{
    // 初始化所有按键的参数
    key_CheckParamInit(&key0_check);
    key_CheckParamInit(&key1_check);
    key_CheckParamInit(&key2_check);
}

/// @brief 按键参数结构体初始化
/// @param check_param 按键参数结构体指针
void key_CheckParamInit(struct keyCheckParam* check_param)
{
    // 按键检测参数初始化
    check_param->key_check_step     = CHECK_PRESS;
    check_param->key_event          = PRESS_UP;
    check_param->func_time_count    = 0;
    check_param->key_press_status   = 0;
    check_param->key_release_status = 0;
}

/// @brief 自定义按键事件检测
/// @param GPIOx 需要进行检测的GPIO端口
/// @param GPIO_Pin 需要进行检测的GPIO引脚
/// @param check_struc 按键事件参数结构体指针
/// @param mode 检测的模式
enum keyEvent key_CheckStatus(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, struct keyCheckParam* check_struc, enum keyCheckMode mode)
{       
    /* 先判断模式，其中：
       模式1时高电平代表按下，低电平代表松开
       模式2时低电平代表按下，高电平代表松开 */
    if(MODE_ONE == mode)
    {
        check_struc->key_press_status   = 1;
        check_struc->key_release_status = 0;
    }
    else if(MODE_TWO == mode)
    {
        check_struc->key_press_status   = 0;
        check_struc->key_release_status = 1;
    }
    
    /* 进入状态机
       开始对相关按键进行检测 */
    switch(check_struc->key_check_step)
    {           
        // 检测按键是否按下
        case CHECK_PRESS:
        {          
            // 判断按键是否按下
            if(check_struc->key_press_status == HAL_GPIO_ReadPin(GPIOx, GPIO_Pin))
            {
                // 按键按下，进入状态2
                check_struc->key_event      = PRESS_DOWN;      // 按键事件标志位设置为按下
                check_struc->key_check_step = CHECK_RELEASE;
            }          
            break;
        }
        case CHECK_RELEASE:
        {
            if(check_struc->key_release_status == HAL_GPIO_ReadPin(GPIOx, GPIO_Pin))
            {
                // 按键松开，进入状态1
                check_struc->key_event      = PRESS_UP;     // 按键事件标志位设置为松开
                check_struc->key_check_step = CHECK_PRESS;  

                // 对已完成的状态机循环进行收尾清理
                check_struc->func_time_count = 0;           // 函数执行次数清零
            }
            else
            {
                /* 间接计时
                   函数执行计数值加1 */
                check_struc->func_time_count++;

                /* 按键未松开，开始长按计时
                   此处的计时是通过程序运行的次数进行间接计时
                   比如：如果函数设置为10ms进入一次，长按3s的计数值就应该为300，以此类推 */
                if(check_struc->func_time_count > 300)
                {
                    check_struc->key_event = LONG_PRESS;  // 按键事件标志位设置为长按                      
                }                   
            }  
            break;
        }
    }
    
    return (check_struc->key_event);   // 返回按键事件枚举值
}


