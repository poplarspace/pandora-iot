/* 文件名称：主任务循环源文件
 * 文件说明：该文件中定义了蜂鸣器相关操作的函数以及变量等 */
#include "buzzer.h"
#include "stm32l4xx_hal.h"
#include "main.h"



void buzzer_Switch(enum buzzerStatus buzzer_status)
{
    switch(buzzer_status){
        case BUZZER_OPEN:
            HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin, GPIO_PIN_SET);
            break;
        case BUZZER_CLOSE:
            HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin, GPIO_PIN_RESET);
            break;
    }
}

