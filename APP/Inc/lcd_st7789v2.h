/* 文件简介：按键事件反馈头文件
 * 文件说明：该文件中定义了按键事件反馈相关的函数和变量 */
#ifndef __LCD_ST7789V2_H__
#define __LCD_ST7789V2_H__
// 头文件
#include "stm32l4xx_hal.h"



// 宏定义
#define	LCD_PWR(n)      (n ? HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_RESET))
#define	LCD_RST(n)		(n ? HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET))
#define	LCD_DC(n)		(n ? HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,GPIO_PIN_RESET))
#define	LCD_CS(n)		(n ? HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_SET) : HAL_GPIO_WritePin(GPIOD,GPIO_PIN_7,GPIO_PIN_RESET))



// 函数声明



#endif  // end of __LCD_ST7789V2_H__




