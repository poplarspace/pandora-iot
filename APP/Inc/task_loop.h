/* 文件名称：主任务循环头文件
 * 文件说明：该文件中声明了主任务循环相关操作的函数以及变量等 */
#ifndef __TASK_LOOP_H__
#define __TASK_LOOP_H__
// 头文件
#include "main.h"



// 函数声明
void task_Init(void);
void task_Loop(void);
  


#endif   // end of __TASH_LOOP_H__


