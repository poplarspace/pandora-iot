/* 文件简介：按键事件反馈头文件
 * 文件说明：该文件中定义了按键事件反馈相关的函数和变量 */
#ifndef __KEY_FEEDBACK_H__
#define __KEY_FEEDBACK_H__



// 函数声明
void key_Key0PressFeedback(void);
void key_Key0LongPressFeedback(void);
void key_Key1PressFeedback(void);
void key_Key1LongPressFeedback(void);
void key_Key2PressFeedback(void);
void key_Key2LongPressFeedback(void);



#endif  // end of __KEY_FEEDBACK_H__


