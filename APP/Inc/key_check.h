/* 文件简介：按键检测头文件
 * 文件说明：该文件中声明了按键检测相关的函数和变量 */
#ifndef __KEY_CHECK_H__
#define __KEY_CHECK_H__
// 头文件
#include "main.h"



// 宏定义
/*---------------------------------------------------------------------------------
说明：
    1. 曝光按键：外部电路是利用光耦来进行电平控制，当按下曝光按键后，相应的GPIO端口检测到的是低电平
    2. 电源按键：外部电路通过短接与否来进行电平控制，当按下电源按键后，相应的GPIO端口检测到的是高电平
    3. 其他按键：外部电路按下后接地，即为低电平
  ---------------------------------------------------------------------------------*/
  // 按键1
#define KEY0_GPIO                           GPIOD     
#define KEY0_GPIO_PIN                       GPIO_PIN_10
// 按键1
#define KEY1_GPIO                           GPIOD     
#define KEY1_GPIO_PIN                       GPIO_PIN_9
// 按键2
#define KEY2_GPIO                           GPIOD     
#define KEY2_GPIO_PIN                       GPIO_PIN_8



// 枚举值
//---------------------------------------------------------------------------------
// 按键相关事件枚举值
enum keyEvent{
	PRESS_UP = 0,       // 按键松开
	PRESS_DOWN,         // 按键按下
    LONG_PRESS          // 按键长按
};
// 按键检测模式枚举值
enum keyCheckMode{
	MODE_ONE = 0,       // 模式1 高电平代表按下，低电平代表松开
	MODE_TWO            // 模式2 低电平代表按下，高电平代表松开
};
// 按键检测步骤枚举值
enum keyCheckStep{ 
    CHECK_PRESS = 0,    // 检测按键是否按下
    CHECK_PRESS_AGAIN,  // 再次确认按键是否按下，消抖
    CHECK_RELEASE,      // 检测按键是否松开
    CHECK_RELEASE_AGAIN // 再次确认按键是否松开，消抖
};
//---------------------------------------------------------------------------------



// 按键检测参数结构体
struct keyCheckParam{ 
    /* 按键检测参数 */
    enum keyCheckStep   key_check_step;        // 定义按键检测步骤枚举值
    enum keyEvent       key_event;             // 定义按键相关事件枚举值    
    uint32_t            func_time_count;       // 函数执行次数，间接用于计时
    uint8_t             key_press_status;      // 按键按下时的电平状态值
    uint8_t             key_release_status;    // 按键松开时的电平状态值       
};



// 外部变量声明
extern struct keyCheckParam     key0_check;
extern struct keyCheckParam     key1_check;
extern struct keyCheckParam     key2_check;



// 函数声明
void          key_EventCheck(void);
void          key_CheckInit(void);
void          key_CheckParamInit(struct keyCheckParam* check_param);
enum keyEvent key_CheckStatus(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, struct keyCheckParam* key_param_struc, enum keyCheckMode mode);



#endif   // end of __KEY_CHECK_H__ 



