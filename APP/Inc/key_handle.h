/* 文件简介：按键事件处理头文件
 * 文件说明：该文件中定义了按键事件处理相关的函数和变量 */
#ifndef __KEY_HANDLE_H__
#define __KEY_HANDLE_H__
// 头文件
#include "stdint.h"
#include "key_check.h"



// 按键事件处理参数结构体
struct keyHandleParam{       
    // 按键事件处理参数
    uint8_t event_handle_ban;                   // 禁止按键事件处理
    uint8_t press_handle_flag;                  // 按键按下事件处理标志位
    uint8_t long_press_handle_flag;             // 按键长按事件处理标志位  
    void (*press_feedback)(void);               // 按键按下事件反馈函数指针
    void (*long_press_feedback)(void);          // 按键长按事件反馈函数指针
};
struct keyDoubleHandleParam{       
    // 双按键事件处理参数
    uint8_t double_event_handle_ban;            // 禁止双按键事件处理
    uint8_t double_press_handle_flag;           // 两个按键同时按下事件处理标志位
    uint8_t double_long_press_handle_flag;      // 两个按键同时长按事件处理标志位  
    void (*double_press_feedback)(void);        // 两个按键同时按下事件反馈函数指针
    void (*double_long_press_feedback)(void);   // 两个按键同时长按事件反馈函数指针
};



// 外部变量声明
extern struct keyHandleParam           key1_handle;       



// 函数声明
void key_EventLoop(void);
void key_Init(void);
void key_HandleInit(void);
void key_HandleParamInit(struct keyHandleParam* handle_struc);
void key_DoubleHandleParamInit(struct keyDoubleHandleParam* double_handle_struc);
void key_EventHandle(struct keyCheckParam check_param, struct keyHandleParam* handle_param);
void key_DoubleEventHandle(struct keyCheckParam check_param1, struct keyCheckParam check_param2, struct keyDoubleHandleParam* double_handle_param);



#endif   // end of __KEY_HANDLE_H__


