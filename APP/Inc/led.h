/* 文件名称：主任务循环头文件
 * 文件说明：该文件中声明了LED相关操作的函数以及变量等 */
#ifndef __LED_H__
#define __LED_H__



// 枚举值
//----------------------------------------------------
// led颜色枚举值
enum ledClour{
    RED,
    GREEN,
    BLUE,
};
// led状态枚举值
enum ledStatus{
    OPEN,
    CLOSE,
};
//----------------------------------------------------



// 函数声明
void led_Show(enum ledClour led_colour, enum ledStatus led_status);
  


#endif   // end of __LED_H__



