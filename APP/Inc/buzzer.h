/* 文件名称：主任务循环头文件
 * 文件说明：该文件中声明了蜂鸣器相关操作的函数以及变量等 */
#ifndef __BUZZER_H__
#define __BUZZER_H__



// 枚举值
//----------------------------------------------------
// 蜂鸣器开关枚举值
enum buzzerStatus{
    BUZZER_OPEN,
    BUZZER_CLOSE,
};



// 函数声明
void buzzer_Switch(enum buzzerStatus buzzer_status);



#endif   // end of __BUZZER_H__

